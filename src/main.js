import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.js"
import App from "@/App";
import HighchartsVue from 'highcharts-vue'
import {index} from "@/store";

createApp(App)
.use(index)
.use(ElementPlus)
.use(HighchartsVue)
.use(router)
.mount('#app')
