const axios = require('axios').default;

async function getDataUser() {
    try {
        const data = await axios.get('https://628324fc92a6a5e4621ea622.mockapi.io/acount')
        return data
    } catch (error) {
        console.log(error)
    }
}
export { getDataUser };