const getters = {
    doneTodos (state) {
        return state.todos.filter(todo => {
            return todo.done
        })
    },
    getTodoById: (state) => (id) => {
        return state.todos.find(todo => todo.id === id)
    },
    data: (state) => (data) => {
        return state.test + data
    }
}
export { getters }