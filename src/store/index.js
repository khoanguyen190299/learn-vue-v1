import {createStore} from "vuex";
import {getters} from "@/store/getters";
import {mutations} from "@/store/mutations";
import {states} from "@/store/states";
import {actions} from "@/store/actions";

const index = createStore({
    state() {
        return states
    },
    mutations: mutations,
    getters: getters,
    actions: actions
})

export { index }