const states = {
    status: true,
    test: 'test:',
    todos: [
        { id: 1, text: '...', done: true },
        { id: 2, text: '...', done: false }
    ]
}

export { states }