import { createWebHistory, createRouter } from "vue-router";
import Onboading from "@/components/Onboading";
import Login from "@/components/Login";
import HomePage from "@/components/HomePage";
import Dashboard from "@/components/Dashboard";
import ListEmployee from "@/components/ListEmployee";
import HomeContent from "@/components/HomeContent";
import SignUp from "@/components/SignUp";
import Order from "@/components/Order";
const routes = [
    {
        path: "/",
        name: "onboading",
        component: Onboading,
    },
    {
        path: "/login/",
        name: "Login",
        component: Login,
    },
    {
        path: "/sign-up/",
        name: "SignUp",
        component: SignUp,
    },
    {
        path: "/home/",
        name: "Home",
        component: HomePage,
        children: [
            {
                name: "Dashboard",
                path: "/dashboard/",
                component: Dashboard,
            },
            {
                name: "ListEmployee",
                path: "/list-employee/",
                component: ListEmployee,
            },
            {
                name: "HomeContent",
                path: "/home/",
                component: HomeContent,
            },
            {
                name: "Order",
                path: "/order/",
                component: Order,
            },
        ],
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
